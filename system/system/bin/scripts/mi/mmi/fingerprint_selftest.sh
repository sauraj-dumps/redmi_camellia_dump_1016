#!/system/bin/sh

#指纹自测试
fingerprint=`getprop |grep persist.vendor.sys.fp.vendor`

if [ "$fingerprint"=="goodix" ]; then
	iftestapk=`pm list package |grep "com.goodix.gftest"`
	if [ -z $iftestapk ]; then
		echo "fail"
		echo "HQ_AutoTest --- $0 --- fingerprintsTest apk not found"
		exit 1
	fi
	rm -f /data/data/com.goodix.gftest/files/GFTest/testtool.txt
	am start -n com.goodix.gftest/com.goodix.gftest.HomeActivity  --es "hqfactory" true --es "performance" false
	sleep 1
	passornot1=`cat data/data/com.goodix.gftest/files/GFTest/testtool.txt |grep -i "pass"`
	ifid1=`cat data/data/com.goodix.gftest/files/GFTest/testtool.txt |grep -i "test_id=1"`
	ifid2=`cat data/data/com.goodix.gftest/files/GFTest/testtool.txt |grep -i "test_id=2"`
	ifid4=`cat data/data/com.goodix.gftest/files/GFTest/testtool.txt |grep -i "test_id=4"`
	echo "licong :$passornot1"
	if [ -n "$passornot1" ] && [ -n "$ifid1" ] && [ -n "$ifid2" ] && [ -n "$ifid4" ]; then
		echo "HQ_AutoTest --- $0 --- fingerprint_self_test(SPI,Reset_pin,Inerrupt_pin) pass"
		exit 0
	else
		echo "HQ_AutoTest --- $0 --- fingerprint_self_test(SPI,Reset_pin,Inerrupt_pin) fail"
		exit 1
	fi
fi
am force-stop  com.goodix.gftest
