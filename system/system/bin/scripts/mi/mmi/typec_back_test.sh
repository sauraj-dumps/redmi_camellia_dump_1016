#!/system/bin/sh

# Set defaults
# J19S-qcom
#node_name="/sys/class/power_supply/usb/otg_online"

# K19-mtk
node_name="/sys/class/power_supply/usb/usb_otg"
node_name_typec="/sys/class/power_supply/usb/typec_cc_orientation"
if [ -f $node_name ];then
	echo "HQ_AutoTest --- $0 --- $node_name file exist"
	cat $node_name
else
	echo "fail"
	echo "HQ_AutoTest --- $0 --- $node_name file no exist"
	exit 1
fi

if [ -f $node_name_typec ];then
	echo "HQ_AutoTest --- $0 --- $node_name_typec file exist"
	val1=`cat $node_name_typec`
	if [ $val2 == "2" ]; then
		echo "HQ_AutoTest ---TYPE_C_BACK pass"
		exit 0
	else
		echo "HQ_AutoTest ---TYPE_C_BACK fail"
		exit 1
	fi
else
	echo "fail"
	echo "HQ_AutoTest --- $0 --- $node_name_typec file no exist"
	exit 1
fi

