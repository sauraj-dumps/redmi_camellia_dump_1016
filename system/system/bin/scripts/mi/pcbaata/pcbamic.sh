#!/system/bin/sh

command1=$1

case $command1 in
    "openmainmic")
	val=`/system/bin/AudioTest 1 5 1`
	if [[ "$val" == *"PASS"* ]];then
	    echo pass
	else
	    echo fail
	fi
    ;;
    "opensubmic")
	val=`/system/bin/AudioTest 2 6 1`
	if [[ "$val" == *"PASS"* ]];then
	    echo pass
	else
	    echo fail
	fi
    ;;
esac
