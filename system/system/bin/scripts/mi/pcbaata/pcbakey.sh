#!/system/bin/sh

testcase=$1
timeout=$2
flag=0

case $testcase in
    "POWER")
	nohup ata_test POWER $timeout>/sdcard/key_result.txt 2>&1 &
	if [ $? -eq 0 ];then
	    echo pass
	else
	    echo fail
	fi
#	while [[ $i -lt $timeout ]]
#	do
#	sleep 1
#	i=$((i+1));
#	content=`cat /sdcard/key_test/result.txt | grep PASS`
#	if [ $? -eq 0 ]; then	    
#	    flag=1
#	    break
#	fi
#	done
#	if [ $flag -eq 1 ]; then	    
#	    echo pass
#	else
#	    echo fail
#	fi
	;;
    "VOLUMEUP")
	nohup ata_test VOLUMEUP $timeout>/sdcard/key_result.txt 2>&1 &
	if [ $? -eq 0 ];then
	    echo pass
	else
	    echo fail
	fi
	;;	
    "VOLUMEDOWN")
	nohup ata_test VOLUMEDOWN $timeout>/sdcard/key_result.txt 2>&1 &
	if [ $? -eq 0 ];then
	    echo pass
	else
	    echo fail
	fi
	;;
    "check")
	content=`cat /sdcard/key_result.txt | grep PASS`
	if [ $? -eq 0 ]; then	    
	    echo pass
	else
	    echo fail
	fi	
	;;
esac

