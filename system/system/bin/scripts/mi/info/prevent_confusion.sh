#!/system/bin/sh
front_camera_sp=`getprop|grep persist.vendor.camera.front |grep -i "not support"`
back_camera_sp=`getprop |grep persist.vendor.camera.main |grep -i "not support"`
depth_camera_sp=`getprop |grep persist.vendor.camera.depth |grep -i "not support"`
macro_camera_sp=`getprop |grep persist.vendor.camera.macro |grep -i "not support"`

echo "Front Camera: $front_camera\n"

echo "Back Camera: $back_camera\n"

echo "Depth Camera: $depth_camera\n"

echo "Macro Camera: $macro_camera\n"

if [ -z "$front_camera_sp" ] && [ -z "$back_camera_sp" ] && [ -z "$depth_camera_sp" ] && [ -z "$macro_camera_sp" ];then
	mainP=`getprop|grep persist.vendor.camera.main | cut -d ":" -f 2 |cut -d '[' -f2 |cut -d ']' -f1 | cut -d "_" -f 1`
	mainL=`getprop|grep persist.vendor.camera.main | cut -d ":" -f 2 |cut -d '[' -f2 |cut -d ']' -f1 | cut -d "_" -f 4`
	depP=`getprop|grep persist.vendor.camera.depth | cut -d ":" -f 2 |cut -d '[' -f2 |cut -d ']' -f1 | cut -d "_" -f 1`
	depL=`getprop|grep persist.vendor.camera.depth | cut -d ":" -f 2 |cut -d '[' -f2 |cut -d ']' -f1 | cut -d "_" -f 4`
	if [ "$mainP" == "$depP" ] && [ "$mainL" == "$depL" ];then
		echo "pass"
		exit 0
	fi
else
	echo "fail"
	exit 1
fi


