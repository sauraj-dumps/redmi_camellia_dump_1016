#!/system/bin/sh

echo "FSN:`getprop |grep ro.ril.oem.sno | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1` \n"

echo "PSN:`getprop |grep ro.ril.oem.psno | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "IMEI1:`getprop |grep ro.ril.oem.imei1 | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "IMEI2:`getprop |grep ro.ril.oem.imei2 | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Build Number:`getprop |grep ro.build.display.id | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Baseband Version:`getprop |grep gsm.version.baseband | cut -d ":" -f 2| cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Kernel Version:"
echo `cat /proc/version |busybox grep -o 'Linux version \S\+'`
echo `cat /proc/version |busybox grep -o '\((\S\+)\)' |cut -d '(' -f2|cut -d ')' -f1`
echo "`cat /proc/version |busybox grep -o '#\S \S\+ \S\+ .\+' |busybox sed -e 's/#\S \S\+ \S\+ //g'`\n"

echo "CPU ID:`cat /proc/serial_num`\n"

echo "HWversion:`getprop |grep ro.boot.hwversion | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

secureboot=`getprop |grep ro.boot.secureboot | cut -d ":" -f 2`
if [ "$secureboot" == "1"  ]; then
	echo "secureboot: yes\n"
else
	echo "secureboot: no\n"
fi
echo "VerifiedBootState:`getprop |grep ro.boot.verifiedbootstate | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Factory ID:`getprop |grep ro.ril.factory_id | cut -d ":" -f 2| cut -d '[' -f2|cut -d ']' -f1`\n"

if [ -e /system/bin/keybox-check ];then
	echo "Keybox: `keybox-check`\n"
else
	echo "Keybox: Not support\n"
fi

ifaa=`getprop |grep persist.vendor.sys.pay.ifaa | cut -d ":" -f 2`
if [ "$ifaa" == "1"  ]; then
	echo "IFAA: Support\n"
else
	echo "IFAA: Not support\n"
fi

if [ -e /system/bin/key_client ];then
	echo "FIDO: `key_client --version fido`\n"
else
	echo "FIDO: Not support\n"
fi

if [ -e /system/bin/key_client ];then
	echo "Widevine: `key_client --version widevine`\n"
else
	echo "Widevine: Not support\n"
fi

echo "BoardID:`cat /sys/class/huaqin/interface/hw_info/pcba_config`\n"

if [ -e /proc/mv ];then
	echo "EMMC fw version:`cat /proc/mv | awk '{print $5}' | tail -n 1`\n"
fi

mem=`cat /proc/mv | awk '{print $2}' | sed -n 2p`
if [ -n "$mem" ]; then
	case $mem in
		"0x01ce") memsup="Samsung"
		;;
		"0x01ad") memsup="Hynix"
		;;
		"0x012c") memsup="Micron"
		;;
		*) echo "don't kown"
		;;
	esac
fi

echo "EMMC: $memsup `cat /proc/mv | awk '{print $3}' | tail -n 1`\n"

echo "DDR: $memsup `cat /proc/mv | awk '{print $3}' | head -n 1`\n"

echo "LCD:`cat /sys/class/huaqin/interface/hw_info/lcm`\n"

echo "TP:`cat /proc/nvt_tp_info`\n"

tpcolor=`cat /proc/tp_lockdown_info | cut -b 5-6`
if [ -n "$tpcolor" ]; then
	case $tpcolor in
		"31") echo "TP surface color: 0x$tpcolor white\n"
		;;
		"32") echo "TP surface color: 0x$tpcolor Black\n"
		;;
		"38") echo "TP surface color: 0x$tpcolor Golden\n"
		;;
		*) echo "TP surface color: 0x$tpcolor Unknown\n"
		;;
	esac
else
	echo "TP surface color: Unknown\n"
fi

echo "TP lockdown info:`cat /proc/tp_lockdown_info`\n"

echo "Front Camera:`getprop|grep persist.vendor.camera.front | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Back Camera:`getprop |grep persist.vendor.camera.main | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Depth Camera:`getprop |grep persist.vendor.camera.depth | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Macro Camera:`getprop |grep persist.vendor.camera.macro | cut -d ":" -f 2 | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Fingerprint Vendor:`getprop |grep persist.vendor.sys.fp.vendor | cut -d ":" -f 2  | cut -d '[' -f2|cut -d ']' -f1`\n"

echo "Acc:`dumpsys sensorservice |grep "android.sensor.accelerometer" |head -n 1 | cut -d ")" -f 2 | cut -d "|" -f 1,2 |sed s/[[:space:]]//g| cut -d '|' -f 2` `dumpsys sensorservice |grep "android.sensor.accelerometer" |head -n 1 | cut -d ")" -f 2 | cut -d "|" -f 1,2 |sed s/[[:space:]]//g| cut -d '|' -f 1`\n"

echo "Gyro:`dumpsys sensorservice |grep "android.sensor.gyroscope" |head -n 1 | cut -d ")" -f 2 | cut -d "|" -f 1,2 |sed s/[[:space:]]//g| cut -d '|' -f 2` `dumpsys sensorservice |grep "android.sensor.gyroscope" |head -n 1 | cut -d ")" -f 2 | cut -d "|" -f 1,2 |sed s/[[:space:]]//g| cut -d '|' -f 1`\n"

echo "Magne:`dumpsys sensorservice |grep "android.sensor.magnetic_field" | head -n 1 | cut -d ")" -f 2 | cut -d "|" -f 1,2 |sed s/[[:space:]]//g| cut -d '|' -f 2` `dumpsys sensorservice |grep "android.sensor.magnetic_field" |head -n 1 | cut -d ")" -f 2 | cut -d "|" -f 1,2 |sed s/[[:space:]]//g| cut -d '|' -f 1`\n"

echo "Alsps:`dumpsys sensorservice |grep "android.sensor.light" |head -n 1 | cut -d ")" -f 2 | cut -d "|" -f 1,2 |sed s/[[:space:]]//g| cut -d '|' -f 2` `dumpsys sensorservice |grep "android.sensor.light" |head -n 1 | cut -d ")" -f 2 | cut -d "|" -f 1,2 |sed s/[[:space:]]//g| cut -d '|' -f 1`\n"
