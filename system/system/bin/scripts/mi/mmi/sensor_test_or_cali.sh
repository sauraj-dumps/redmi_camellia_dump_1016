#!/system/bin/sh

# Set defaults
command_1=$1
command_2=$2
command_3=$3

bin_name="sns_sensors_test_and_cal"
bin=`which $bin_name`
if [[ $bin != *$bin_name* ]]; then
	echo "sns_sensors_test_and_cal executable does not exist"
	echo "HQ_AutoTest --- $0 --- sns_sensors_test_and_cal executable does not exist"
	exit 1
fi

case $command_1 in
	"sensortest")
		case $command_2 in
			"Mag")
				sns_sensors_test_and_cal -c 2,1,50000000,0
				;;
			"Acc")
				sns_sensors_test_and_cal -c 1,1,50000000,0
				;;
			"Gyr")
				sns_sensors_test_and_cal -c 4,1,50000000,0
				;;
			"Lux")
				sns_sensors_test_and_cal -c 5,1,50000000,0
				;;
			"Lux_back")
				sns_sensors_test_and_cal -c 75,1,200000000,0
				;;
			"Prx")
				sns_sensors_test_and_cal -c 8,7,50000000,0
				;;
			"Prx_state")
				sns_sensors_test_and_cal -c 8,1,50000000,0
				;;
			"Bar")
				echo "$command_2 not support"
				;;
			"SAR")
				attribute_name="ro.boot.hwc"
				var=`getprop $attribute_name`
				if [ "Global" == $var ];then
					sns_sensors_test_and_cal -c 72,1,50000000,0
				else
					echo "$var not support SAR"
				fi
				;;
			"hall")
				echo "$command_2 not support"
				;;
			"slidehall")
				echo "$command_2 not support"
				;;
			"NFC")
				attribute_name="ro.build.product"
				var=`getprop $attribute_name`
				if [ "camellian" == $var ];then
					val=`dumpsys nfc | grep mState |wc -l`
					if [ "1" == $val ];then
						echo "pass"
					else
						echo "fail"
					fi
				else
					echo "not support"
				fi
				;;
			*)
				echo "$command_2 not support"
				;;
		esac
		;;
	"sensorcalibration")
		case $command_2 in
			"Mag")
				sns_sensors_test_and_cal -c 2,3,50000000,0
				;;
			"Acc")
				sns_sensors_test_and_cal -c 1,3,50000000,0
				;;
			"Gyr")
				sns_sensors_test_and_cal -c 4,3,50000000,0
				;;
			"Lux")
				sns_sensors_test_and_cal -c 5,3,1,0
				;;
			"Lux_data")
				coe=`cat mnt/vendor/nvcfg/sensor/als_cali.json | awk -F "," '{print $1}' \
				    | sed -n 3p`
				res=$(printf "%.3f" `echo "scale=3; $coe/1000"|bc`)
				echo "Success! Lsensor: Cal Value coe = ${res}"
				;;
			"Lux_leak")
				sns_sensors_test_and_cal -c 5,3,2,0
				;;
			"Lux_leak_data")
				sns_sensors_test_and_cal get_leak_light_bias 3
				;;
			"Lux_back")
				sns_sensors_test_and_cal -c 75,3,1,0
				;;
			"Lux_data_back")
				coe=`cat mnt/vendor/nvcfg/sensor/rear_als_cali.json | awk -F "," '{print $1}' \
				    | sed -n 3p`
				res=$(printf "%.3f" `echo "scale=3; $coe/1000"|bc`)
				echo "Success! Lsensor: Cal Value coe = ${res}"
				;;
			"Prx")
				case $command_3 in
					"cover")
						sns_sensors_test_and_cal -c 8,3,3,0
						;;
					"nocover")
						sns_sensors_test_and_cal -c 8,3,4,0
						;;
					*)
						echo "$command_3 not support"
						echo "HQ_AutoTest --- $0 --- $command_3 not support"
						;;
				esac
				;;
			"Prx_data")
				ct=`cat mnt/vendor/nvcfg/sensor/ps_cali.json | awk -F "," '{print $1}' \
				   | awk '{print $1}' | sed -n 7p`
				cm=`cat mnt/vendor/nvcfg/sensor/ps_cali.json | awk -F "," '{print $1}' \
				   | awk '{print $1}' | sed -n 5p`
				delta=`cat mnt/vendor/nvcfg/sensor/ps_cali.json | awk -F "," '{print $1}' \
				   | awk '{print $1}' | sed -n 8p`
				echo "Success! Psensor: CT = $ct   delta = $delta  3cm = $cm"
				;;
			"SAR")
				attribute_name="ro.boot.hwc"
				var=`getprop $attribute_name`
				if [ "Global" == $var ];then
					sns_sensors_test_and_cal -c 72,3,50000000,0
				else
					echo "$var not support SAR"
				fi
				;;
			*)
				echo "$command_2 not support"
				echo "HQ_AutoTest --- $0 --- $command_2 not support"
				;;
		esac
		;;
	*)
		echo "$command_1 not support"
		echo "HQ_AutoTest --- $0 --- $command_1 not support"
		;;
esac
