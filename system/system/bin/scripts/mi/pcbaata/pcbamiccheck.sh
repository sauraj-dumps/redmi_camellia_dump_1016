#!/system/bin/sh

command1=$1
db_ref=10

if [ $# -gt 2 ]; then
    db_ref=$2
fi

case $command1 in
    "mainmic")
	wav=/sdcard/mainmiccapture.wav
    ;;
    "submic")
	wav=/sdcard/submiccapture.wav
    ;;
esac

check_result=`/vendor/bin/check_wav $wav $2 | awk '{print $1}'`

avg=${check_result#*:}
avg2=${avg%%m*}

if [ $avg2 -gt 0 ];then
    echo pass
else
    echo fail
fi
