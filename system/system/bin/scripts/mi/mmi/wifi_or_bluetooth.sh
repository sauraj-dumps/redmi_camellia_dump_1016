#!/system/bin/sh

# Set defaults
command_1=$1
command_2=$2

platform=mtk

case $command_1 in
	"wifi")
		case $command_2 in
			"status")
				svc wifi status
				;;
			"enable")
				svc wifi enable
				#ifconfig wlan0 up
				;;
			"scan")
				if [ $platform == "qcom" ]; then
					wpa_cli scan
					#iw dev wlan0 scan
				else
					svc wifi scan
				fi
				;;
			"scan_details")
				if [ $platform == "qcom" ]; then
					wpa_cli scan_results
				else
					svc wifi scan_results
				fi
				;;
			"disable")
				svc wifi disable
				#ifconfig wlan0 down
				;;
			"mac")
				settings get secure bluetooth_address
				;;
				
			*)
				echo "HQ_AutoTest --- $command_2 not support"
				echo "$command_2 not support"
				;;
		esac
		;;
	"bluetooth")
		case $command_2 in
			"status")
				svc bluetooth status
				;;
			"enable")
				svc bluetooth enable
				;;
			"scan")
				svc bluetooth scan
				;;
			"scan_details")
				svc bluetooth scan_details
				;;
			"disable")
				svc bluetooth disable
				;;
			"mac")
				bluetooth_address=`settings get secure bluetooth_address`
				if [ "$bluetooth_address" == "" ]; then
					echo "MAC: $bluetooth_address"
				else
					echo "MAC: $bluetooth_address"
				fi
				;;
			*)
				echo "HQ_AutoTest --- $command_2 not support"
				echo "$command_2 not support"
				;;
		esac
		;;
	*)
		echo "HQ_AutoTest --- $command_1 not support"
		echo "$command_1 not support"
		;;
esac
