#!/system/bin/sh

echo 255 >/sys/class/leds/lcd-backlight/brightness 
ifheadset=`cat /sys/class/switch/h2w/state`

if [ "$ifheadset" == "0" ]; then
	echo "please input headset"
	echo "HQ_AutoTest --- no headset --- fail"
	exit 1
fi

echo "waiting..."
/vendor/bin/fmtest > /sdcard/fm_result.txt
cat /sdcard/fm_result.txt
result=`cat /sdcard/fm_result.txt | grep -i fail`
if [ -n "$result" ]; then
	echo $result
	rm /sdcard/fm_result.txt
	exit 1
else
	echo "HQ_AutoTest --- $0 --- fm --- pass"
	rm /sdcard/fm_result.txt
	exit 0
fi
