#!/system/bin/sh

# Set defaults
command=$1

case $command in
  "fingerprint")
#    node_name="sys/class/huaqin/interface/hw_info/fingerprint"
#    values=`cat $node_name`
    prop="persist.vendor.sys.fp.vendor"
    values=`getprop $prop`
    if [[ $values == "goodix" ]] || [[ $values == "fpc" ]]; then
      echo "pass"
    else
      echo "fail"
    fi
    ;;
  "lcd")
    ata_lcd
    ;;
  "tp")
    node_name="/proc/bus/input/devices"
    values1=`cat $node_name | grep "mtk-tpd" | wc -l`
    values2=`cat $node_name | grep "mtk-tpd2" | wc -l`
    values3=`cat $node_name | grep "NVTCapacitiveTouchScreen" | wc -l`
    if [[ $values1 == 1 ]] || [[ $values2 == 1 ]] || [[ $values3 == 1 ]]; then
      echo "pass"
    else
      echo "fail"
    fi
    ;;
  "receiver")
    echo "HQ_AutoTest --- $command --- start play"
    AudioTestPcba 3 3 4
    if [ $? -ne 0 ];then
      echo "fail"
      exit 2
    fi
      echo "HQ_AutoTest --- $command --- end "
      echo "pass"
    ;;
  "speaker")
    echo "HQ_AutoTest --- $command --- start play"
    AudioTestPcba 3 3 3
    if [ $? -ne 0 ];then
      echo "fail"
      exit 2
    fi
      echo "HQ_AutoTest --- $command --- end"
      echo "pass"
    ;;
  *)
    echo "$command not support"
    ;;
esac
