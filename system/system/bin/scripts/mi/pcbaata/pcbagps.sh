#!/system/bin/sh

command1=$1
command2=$2
num=0
flagGps=0

case $command1 in
    "gps")
	case $command2 in
	    "search")
		val=`am start -n com.huaqin.ata.gps/.MainActivity 2>&1 | grep "Error" | wc -l`
               	if [ $val != "0" ]; then
                    echo "fail"
		    exit 0
               	fi

		while [ $flagGps -eq 0 ] && [ $num -lt 10 ]
		do
		    val=`settings get secure location_providers_allowed`
		    if [[ -n $val ]];then
			flagGps=1
		    fi
		    sleep 1
		    num=$((num+1))
		done
		
		if [ $flagGps -eq 0 ];then
		    echo "fail"
		    exit 0	
		fi
		
		i=1;
		flagSearch=0;

		while [ $i -le 60 ]
		do
		    sleep 3
		    i=`expr $i + 3`
			
			
		    if [ -f "/sdcard/gps_test/satellite.txt" ]; then
            		content=`cat /sdcard/gps_test/satellite.txt`
			if [ -n "$content" ];then
			    flagSearch=1
                            break
			fi
       		    fi
		done

		if [ $flagSearch == 0 ];then
                    am force-stop com.huaqin.ata.gps
		    echo "fail"
		elif [ $flagSearch == 1 ];then
		    echo "pass"
		fi
	    ;;
	esac
    ;;
esac
