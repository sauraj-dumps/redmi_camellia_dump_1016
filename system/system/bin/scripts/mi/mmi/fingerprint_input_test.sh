#!/system/bin/sh

#指纹录入测试
fingerprint=`getprop |grep persist.vendor.sys.fp.vendor`

if [ "$fingerprint"=="goodix" ]; then
	iftestapk=`pm list package |grep "com.goodix.gftest"`
	if [ -z $iftestapk ]; then
		echo "fail"
		echo "HQ_AutoTest --- $0 --- fingerprintsTest apk not found"
		exit 1
	fi
	rm -f /data/data/com.goodix.gftest/files/GFTest/testtool.txt
	am start -n com.goodix.gftest/com.goodix.gftest.HomeActivity  --es "hqfactory" true --es "performance" true
	echo "Please press the fingerprint,5 second"
	sleep 5
	ifenroll=`cat data/data/com.goodix.gftest/files/GFTest/testtool.txt |grep -i "test_id=7"`
	passornot2=`cat data/data/com.goodix.gftest/files/GFTest/testtool.txt |grep -i "pass"`
	if [ -n "$ifenroll" ] && [ -n "$passornot2" ]; then
		echo "HQ_AutoTest --- $0 --- fingerprint_test_enroll pass"
		exit 0
	else
		echo "HQ_AutoTest --- $0 --- fingerprint_test_enroll fail"
		exit 1
	fi
fi
am force-stop  com.goodix.gftest
