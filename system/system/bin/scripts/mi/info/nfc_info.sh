#!/system/bin/sh

# Set defaults
command=$1

case $command in
	"nfccplc")
		echo "not support"
		echo "HQ_AutoTest --- $0 --- $command --- nfc not support"
		;;
	"nfcdieid")
		attribute_name="ro.build.product"
		var=`getprop $attribute_name`
		if [ "camellian" == $var ];then
		  val=`stfactorydemo-vendor 7 2> /dev/null | grep "CLF Unique s/n:" | awk '{print $4}'`
			if [ -n "$val" ];then
				echo $val
			else
				echo "fail"
			fi
		else
			echo "not support"
			echo "HQ_AutoTest --- $0 --- $command --- nfc not support"
		fi
		;;
	*)
		echo "$command not support"
		echo "HQ_AutoTest --- $0 --- $command not support"
		;;
esac
