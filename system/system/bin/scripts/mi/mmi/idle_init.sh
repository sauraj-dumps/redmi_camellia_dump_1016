#!/system/bin/sh

am broadcast -a action.screen.timeout.test --ez test_flag true >/dev/null 2>&1
usleep 50
settings put global airplane_mode_on 1 >/dev/null 2>&1
usleep 50
am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true >/dev/null 2>&1
echo "pass"