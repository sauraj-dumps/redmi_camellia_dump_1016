#!/system/bin/sh

#BACKLIGHT_NOTE="/sys/class/leds/lcd-backlight/brightness"
#BACKLIGHT_NOTE="/sys/class/backlight/panel0-backlight/brightness"

usage(){
        echo "Usage: $0 unlock"
        exit 1
}

if [ $# -ne 1 ]; then
        usage
else
        if [ "$1" != "unlock" ]; then
                usage
                exit 1
        fi
fi

input keyevent 82 2>/dev/null
usleep 500000
input keyevent 82 2>/dev/null
input keyevent 82 2>/dev/null
usleep 500000
val1=`dumpsys window policy | grep showing=false | wc -l`
if [ "1" == $val1 ];then
    echo "pass"
    exit 0
else
    input swipe 535 1880 535 180 100
    usleep 500000
    val2=`dumpsys window policy | grep showing=false | wc -l`
    echo "HQ_AutoTest --- $1 ---dumpsys result: $val2"
    echo "pass"
    exit 0
fi
