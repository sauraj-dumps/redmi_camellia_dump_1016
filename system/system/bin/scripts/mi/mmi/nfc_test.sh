#!/system/bin/sh

# Set defaults
command=$1

bin_name="stfactorydemo-vendor"
bin=`which $bin_name`
if [[ $bin != *$bin_name* ]]; then
      echo "stfactorydemo-vendor executable does not exist"
      echo "HQ_AutoTest --- $0 --- stfactorydemo-vendor executable does not exist"
      exit 1
fi

case $command in
  "init")
    val=`stfactorydemo-vendor 0 2> /dev/null | grep "chr_succeed" | wc -l`
    if [ $val == "1" ]; then
      echo "pass"
    else
      echo "fail"
    fi
    ;;
  "cardemulation")
    svc nfc disable
    sleep 1
    val=`stfactorydemo-vendor 1 2> /dev/null | grep "FAILED" | wc -l`
    if [ $val == "0" ]; then
      input keyevent 82 2>/dev/null
      sleep 1
      svc nfc enable
      sleep 3
      val1=`dumpsys nfc | grep "mState=on" | wc -l`
      if [ $val1 == "1" ]; then
        echo "pass"
      else
        echo "fail"
      fi
    else
      echo "fail"
    fi
    ;;
  "cardreader")
    svc nfc disable
    sleep 1
    val=`stfactorydemo-vendor 6 2> /dev/null | grep "FAILED" | wc -l`
    if [ $val == "0" ]; then
      echo "pass"
    else
      echo "fail"
    fi
    ;;
  "close")
    svc nfc disable
    sleep 3
    val=`dumpsys nfc | grep "mState=on" | wc -l`
    if [ $val == "0" ]; then
      echo "pass"
    else
      echo "fail"
    fi
    ;;
  *)
    echo "$command not support"
    ;;
esac