## camellia-userdebug 11 RP1A.200720.011 1599 test-keys
- Manufacturer: xiaomi
- Platform: mt6833
- Codename: camellia
- Brand: Redmi
- Flavor: camellia-userdebug
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1599
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/camellia/camellia:11/RP1A.200720.011/1599:userdebug/test-keys
- OTA version: 
- Branch: camellia-userdebug-11-RP1A.200720.011-1599-test-keys
- Repo: redmi_camellia_dump_1016


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
