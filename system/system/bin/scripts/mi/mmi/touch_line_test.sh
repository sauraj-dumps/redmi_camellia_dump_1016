#!/system/bin/sh

if [ -f "/sdcard/touchtest/touchtest_result.txt" ]; then
	rm -r /sdcard/touchtest/
fi

val=`am start -n com.huaqin.touchtest/.MainActivity 2>&1 | grep "Error" | wc -l`
if [ $val != "0" ]; then
	echo "touchtest apk can't intent,test fail"
	exit 1
fi

sleep 30
am force-stop com.huaqin.touchtest

if [ -f "/sdcard/touchtest/touchtest_result.txt" ]; then
		content=`cat /sdcard/touchtest/touchtest_result.txt|grep -i pass`
	if [ -n "$content" ]; then
			echo "Touch Panel Pass"
			if [ -f "/sdcard/touchtest/touchtest_result.txt" ]; then
				rm -r /sdcard/touchtest/
			fi
			exit 0
		else
			echo "Touch Panel fail"
			if [ -f "/sdcard/touchtest/touchtest_result.txt" ]; then
				rm -r /sdcard/touchtest/
			fi
			exit 1
	fi
else
	echo "not found /sdcard/touchtest/touchtest_result.txt"
	exit 1
fi
