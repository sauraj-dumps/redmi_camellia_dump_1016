#!/system/bin/sh

# Set defaults
# J19S-qcom
#node_name="/sys/class/power_supply/usb/otg_online"

# K19-mtk
node_name="/sys/class/power_supply/usb/usb_otg"

if [ -f $node_name ];then
	echo "HQ_AutoTest --- $0 --- $node_name file exist"
	cat $node_name
	exit 0
else
	echo "fail"
	echo "HQ_AutoTest --- $0 --- $node_name file no exist"
	exit 1
fi
