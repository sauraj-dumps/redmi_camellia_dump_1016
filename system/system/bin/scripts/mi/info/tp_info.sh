#!/system/bin/sh

# Set defaults
command=$1

node_name="/proc/tp_lockdown_info"

if [ -f $node_name ];then
	echo "HQ_AutoTest ---$0 --- $command --- $node_name file exist"
	case $command in
	"tplockdowninfo")
		cat $node_name
		;;
	"tpcolor")
		color=`cat $node_name`
		case ${color:4:2} in
		"31")
			echo "white"
			;;
		"32")
			echo "black"
			;;
		*)
			echo "fail"
			;;
		esac
		;;
	*)
		echo "$command not support"
		echo "HQ_AutoTest ---$0 --- $command not support"
		;;
	esac
else
	echo "HQ_AutoTest ---$0 --- $command --- $node_name file no exist"
	echo "$node_name file no exist"
fi
